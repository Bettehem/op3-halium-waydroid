This repo has a custom built `halium-boot.img` for the OnePlus 3(T) which has Waydroid modules in the kernel and fixes so you no longer need to flash `halium-ramdisk.zip`.
Also fixes an issue where if the device is powered off and you connect a charger, the device would just boot into fastboot mode and not actually charge the device.
